import java.util.Random;

public class Task {
    private final String title;
    private final Random random;
    private int counter;
    private boolean completed;
    private final int loopNum;
    private int priority;

    public Task(String title, int loopNum) {
        this.title = title;
        random = new Random();
        counter = 0;
        completed = false;
        this.loopNum = loopNum;
        priority = 1;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isCompleted() {
        return completed;
    }

    public synchronized void work(int time) {
        int start = 0;
        for (int i = counter; i < loopNum; i++) {
            try {
                start += random.nextInt(50);
                Thread.sleep(start);
                System.out.printf("%-10.7s %-15.12s %-15.10s \n", title + " " + (i + 1), "start = " + start, "time = " + time);
                if (i == (loopNum - 1)) {
                    completed = true;
                    System.out.printf("%-10.7s %-15.10s \n", title, "COMPLETED!!");
                    return;
                }
                if (time * priority < start) {
                    counter = i + 1;
                    return;
                }
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
