import java.util.ArrayList;
import java.util.List;

public class TaskPool {
    private final List<Task> tasks;
    private final int taskDelay;

    public TaskPool(){
        tasks = new ArrayList<>();
        taskDelay = 20;
    }

    public void addTask(Task task) {
        tasks.add(task);
    }

    public void run() {
        while (true) {
            boolean ifAllCompleted = true;
            for (Task task : tasks) {
                if (task.isCompleted()) {
                    continue;
                }
                ifAllCompleted = false;
                task.work(taskDelay);
            }
            if (ifAllCompleted) break;
        }
    }
}
