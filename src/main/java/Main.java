import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        //создаем и заполняем список задач
        List<Task> tasks = new ArrayList<>();

        int tasksNum = 3;       //количество задач
        int loopNum = 5;       //количество итераций в каждой задаче

        for (int i = 0; i < tasksNum; i++) tasks.add(new Task("Task " + (i + 1),loopNum));

        //задаем приоритет выполнения задач
        tasks.get(2).setPriority(10);

        //создаем пул задач и добавляем задачи
        TaskPool taskPool = new TaskPool();
        for (Task task : tasks) taskPool.addTask(task);

        //запускаем выполнение задач
        taskPool.run();
    }
}
